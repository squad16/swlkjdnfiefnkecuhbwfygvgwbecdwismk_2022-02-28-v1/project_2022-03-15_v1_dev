click
Sphinx
coverage
flake8
python-dotenv>=0.5.1
pyspark==2.4.4
pytest
# Dependency to generate excel file for exploitation
#excel_generator_mf==0.0.14
#pydantic-yaml

